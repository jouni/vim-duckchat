if vim.g.loaded_duckchat then
	return
end
vim.g.loaded_duckchat = true

local curl = vim.fn.exepath("curl")
if not curl or curl == '' then
	print("curl not in $PATH")
	return
end

local duckchat = require('duckchat')

if vim.g["duckchat#model"] == nil then
	vim.g["duckchat#model"] = "gpt-4o-mini"
end

if vim.g["duckchat#auto_update_log"] == nil then
	vim.g["duckchat#auto_update_log"] = 1
end

if vim.g["duckchat#close_msg_on_send"] == nil then
	vim.g["duckchat#close_msg_on_send"] = 1
end

local opts_to_lines = function(opts)
	if opts.range == 0 then
		return {}
	end
	return vim.api.nvim_buf_get_lines(0, opts.line1 - 1, opts.line2, {})
end

vim.api.nvim_create_user_command('DuckChatMsgQuick', duckchat.new_msg_quick, {})
vim.api.nvim_create_user_command('DuckChatMsg', function(opts)
	duckchat.new_msg(opts_to_lines(opts))
end, { range = true })

vim.api.nvim_create_user_command('DuckChatMsgOneshotQuick', duckchat.new_msg_oneshot_quick, {})
vim.api.nvim_create_user_command('DuckChatMsgOneshot', function(opts)
	duckchat.new_msg_oneshot(opts_to_lines(opts))
end, { range = true })

vim.api.nvim_create_user_command('DuckChatLogToggle', duckchat.log_toggle, {})
vim.api.nvim_create_user_command('DuckChatRedo', duckchat.redo, {})
vim.api.nvim_create_user_command('DuckChatClear', duckchat.clear, {})
