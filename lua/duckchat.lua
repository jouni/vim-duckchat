local api = vim.api

local api_url = 'https://duckduckgo.com/duckchat/v1'
local status_cmd = {
	'curl', api_url .. '/status',
	'-s', '-H', 'x-vqd-accept: 1', '-w', '%header{x-vqd-4}', '-X', 'HEAD'
}
local chat_cmd = {
	'curl', api_url .. '/chat',
	'-s', '--json', '@-', '-w', '%header{x-vqd-4}', "--variable", '%VQD', '--expand-header', 'x-vqd-4:{{VQD}}'
}

local g_vqd = nil
local g_last_vqd = nil
local g_messages = {}

local duckchat = {}

local function iter_lines(s)
        if s:sub(-1)~="\n" then s=s.."\n" end
        return s:gmatch("(.-)\n")
end

local function api_status()
	local obj = vim.system(status_cmd, { stderr = false, text = true }):wait()
	return obj.stdout
end

local function create_scratch()
	local buf = api.nvim_create_buf(false, true)
	api.nvim_buf_set_option(buf, "bufhidden", "delete")
	return buf
end

local function log_is_loaded()
	local buf = vim.t.duckchat_log
	return buf and buf ~= 0 and api.nvim_buf_is_loaded(buf)
end

local function reload_log()
	if log_is_loaded() then
		local buf = vim.t.duckchat_log

		local lines = {}
		for _, v in ipairs(g_messages) do
			if v["role"] == "user" then
				table.insert(lines, "===USER===")
			elseif v["role"] == "assistant" then
				table.insert(lines, "===ASSISTANT===")
			end

			for i in iter_lines(v["content"]) do
				table.insert(lines, i)
			end

			table.insert(lines, "===END===")
			table.insert(lines, "")
		end

		api.nvim_buf_set_option(buf, "modifiable", true)
		api.nvim_buf_set_lines(buf, 0, -1, true, lines)
		api.nvim_buf_set_option(buf, "modifiable", false)
	end
end

local function api_chat_on_exit(obj)
	local done = nil
	local msg = ""

	for i in iter_lines(obj.stdout) do
		if i and i ~= "" then
			if done then
				table.insert(g_messages, {
					role = "assistant",
					content = msg
				})

				g_last_vqd = g_vqd
				g_vqd = i

				if vim.g["duckchat#auto_update_log"] then
					reload_log()
				end

				break
			end

			local l = i:sub(6):match("^%s*(.-)%s*$")
			if l == "[DONE]" then
				done = 1
			else
				local m = vim.json.decode(l)["message"]
				msg = msg .. (m and m or "")
			end
		end
	end
end

local function api_chat_oneshot_on_exit(obj)
	local msg = ""
	for i in iter_lines(obj.stdout) do
		if i and i ~= "" then
			local l = i:sub(6):match("^%s*(.-)%s*$")
			if l == "[DONE]" then
				api.nvim_echo({{msg, 'None'}}, true, {})
				break
			end
			local m = vim.json.decode(l)["message"]
			msg = msg .. (m and m or "")
		end
	end
end

local function api_chat(messages, vqd, on_exit)
	local vqd = vqd and vqd or api_status()
	local obj = vim.system(chat_cmd, {
		stderr = false, text = true, env = { VQD = vqd }, stdin = true
	}, on_exit)
	obj:write(vim.json.encode({ model = vim.g["duckchat#model"], messages = messages }))
	obj:write(nil)
end

local new_msg_internal = function(lines, send_func)
	local buf = vim.t.duckchat_msg

	if not buf or buf == 0 or not api.nvim_buf_is_loaded(buf) then
		local win = api.nvim_open_win(0, true, { split = "left" })
		buf = create_scratch()
		vim.t.duckchat_msg = buf
		api.nvim_win_set_buf(win, buf)
	end

	api.nvim_buf_set_lines(buf, 0, -1, true, lines and lines or {})
	api.nvim_buf_set_name(buf, "[duckchat message]")
	api.nvim_buf_create_user_command(buf, 'DuckChatSend', send_func, {})
end

duckchat.new_msg = function(lines)
	new_msg_internal(lines, function()
		local buf = vim.t.duckchat_msg
		local lines = api.nvim_buf_get_lines(buf, 0, -1, {})

		if vim.g["duckchat#close_msg_on_send"] then
			api.nvim_buf_delete(buf, {})
		end

		table.insert(g_messages, {
			role = "user",
			content = table.concat(lines, "\n")
		})
		api_chat(g_messages, g_vqd, vim.schedule_wrap(api_chat_on_exit))
	end)
end

duckchat.new_msg_oneshot = function(lines)
	new_msg_internal(lines, function()
		local buf = vim.t.duckchat_msg
		local lines = api.nvim_buf_get_lines(buf, 0, -1, {})

		api_chat({{
			role = "user",
			content = table.concat(lines, "\n")
		}}, nil, vim.schedule_wrap(api_chat_oneshot_on_exit))
	end)
end

local new_msg_quick_internal = function(input_func)
	vim.ui.input({ prompt = "Message: " }, function(input)
		if input then
			input_func(input)
		end
	end)
end

duckchat.new_msg_quick = function()
	new_msg_quick_internal(function(input)
		table.insert(g_messages, {
			role = "user",
			content = input
		})
		api_chat(g_messages, g_vqd, vim.schedule_wrap(api_chat_on_exit))
	end)
end

duckchat.new_msg_oneshot_quick = function()
	new_msg_quick_internal(function(input)
		api_chat({{
			role = "user",
			content = input
		}}, nil, vim.schedule_wrap(api_chat_oneshot_on_exit))
	end)
end

duckchat.log_toggle = function()
	local buf = vim.t.duckchat_log

	if log_is_loaded() then
		api.nvim_buf_delete(buf, {})
	else
		local win = api.nvim_open_win(0, true, { split = "left" })

		buf = create_scratch()
		vim.t.duckchat_log = buf

		api.nvim_buf_set_name(buf, "[duckchat log]")
		reload_log()
		api.nvim_win_set_buf(win, buf)
	end
end

duckchat.redo = function()
	if #g_messages > 1 then
		g_vqd = g_last_vqd
		table.remove(g_messages)
		api_chat(g_messages, g_vqd, vim.schedule_wrap(api_chat_on_exit))
	end
end

duckchat.clear = function()
	g_vqd = nil
	g_messages = {}
end

return duckchat
